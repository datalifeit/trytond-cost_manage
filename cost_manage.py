# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin, tree
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.transaction import Transaction
from sql import Null

SEPARATOR = ' / '


class CostAllocationMixin(object):

    company = fields.Many2One('company.company', 'Company', required=True,
        select=True)
    type_ = fields.Many2One('cost.manage.cost.allocation.procedure',
        'Type', required=True, select=True)
    source = fields.Reference('Cost source',
        selection='get_source_name', required=True, select=True)
    amount = fields.Numeric('Cost amount',
        digits=(16, Eval('currency_digits', 2)), required=True,
        depends=['currency_digits'])
    currency = fields.Function(
            fields.Many2One('currency.currency', 'Currency'),
            'get_currency')
    currency_digits = fields.Function(
            fields.Integer('Currency Digits'),
            'get_currency_digits')
    date = fields.Date('Date', required=True, select=True)
    category = fields.Many2One('cost.manage.category',
        'Cost category', required=True, readonly=True, select=True,
        ondelete='RESTRICT', domain=[
            ('childs', '=', None)
        ])
    root_category = fields.Many2One('cost.manage.category', 'Root Category',
        required=True, ondelete='RESTRICT', select=True,
        domain=[('parent', '=', None)])
    source_code = fields.Function(
        fields.Char('Source code'),
        'get_source_data', searcher='search_source_code')
    source_concept = fields.Function(
        fields.Char('Source concept'), 'get_source_data')

    def get_currency(self, name):
        return self.company.currency.id

    def get_currency_digits(self, name):
        return self.currency.digits

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Category = pool.get('cost.manage.category')

        categories = {}
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if values['category'] not in categories:
                root_cat = Category(values.get('category')).root
                categories[values.get('category')] = \
                    root_cat and root_cat.id or values['category']
            values['root_category'] = categories[values.get('category')]
        return super().create(vlist)

    @classmethod
    def _get_source_name(cls):
        return ['']

    @classmethod
    def get_source_name(cls):
        Model = Pool().get('ir.model')
        models = cls._get_source_name()
        models = Model.search([
                ('model', 'in', models),
                ])
        return [('', '')] + [(m.model, m.name) for m in models]

    @classmethod
    def get_source_data(cls, records, names):
        record_ids = {r.id: None for r in records}
        res = {name: record_ids.copy() for name in names}
        return res

    @classmethod
    def _order_source_code_column(cls, tables):
        return Null

    @classmethod
    def order_source_code(cls, tables):
        return [cls._order_source_code_column(tables)]

    @classmethod
    def source_code_search_domain(cls, name, clause):
        return [('id', '<', 0)]

    @classmethod
    def search_source_code(cls, name, clause):
        return ['OR', *cls.source_code_search_domain(name, clause)]


class CostSourceMixin(object):

    cost_category = fields.Function(
        fields.Many2One('cost.manage.category', 'Cost category'),
        'get_cost_category')
    cost_override = fields.Boolean('Cost override', readonly=True)

    def get_cost_category(self, name):
        pass

    @staticmethod
    def default_cost_override():
        return True


class CostCategory(DeactivableMixin, tree(separator=SEPARATOR),
        ModelSQL, ModelView):
    '''Cost Manage Category'''
    __name__ = 'cost.manage.category'

    name = fields.Char('Name', translate=True)
    parent = fields.Many2One('cost.manage.category', 'Parent',
        select=True, left='left', right='right', ondelete="RESTRICT")
    childs = fields.One2Many('cost.manage.category', 'parent',
        'Children')
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)

        table = cls.__table_handler__(module_name)
        if table.column_exist('kind'):
            table.drop_column('kind')

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    @classmethod
    def validate(cls, categories):
        super().validate(categories)
        for category in categories:
            category.check_name()

    @property
    def root(self):
        if not self.parent:
            return self
        with Transaction().set_context(active_test=False):
            categories, = self.__class__.search([
                    ('parent', '=', None),
                    ('parent', 'parent_of', [self.id])
                ]) or [None]
        return categories

    def check_name(self):
        if SEPARATOR in self.name:
            raise UserError(
                gettext('cost_manage.msg_cost_manage_category_wrong_name',
                    category=self.name, separator=SEPARATOR))

    @classmethod
    def _check_child_use_models(cls):
        return []

    @classmethod
    def check_child_use(cls, records):
        pool = Pool()
        IrModel = pool.get('ir.model')
        parents = [record.parent for record in records if record.parent]

        for model_name in cls._check_child_use_models():
            Model = pool.get(model_name)
            _records = Model.search([('cost_category', 'in', parents)])
            if _records:
                categories = [record.cost_category.rec_name
                    for record in _records]
                ir_model, = IrModel.search([('model', '=', model_name)])
                raise UserError(gettext(
                    'cost_manage.msg_cost_category_exists_in_model',
                    categories=', '.join(categories),
                    model=ir_model.rec_name))

    @classmethod
    def write(cls, *args):
        super().write(*args)

        actions = iter(args)
        to_check = []
        for records, values in zip(actions, actions):
            if 'parent' in values:
                to_check.extend(records)
        if to_check:
            cls.check_child_use(to_check)

    @classmethod
    def create(cls, vlist):
        records = super().create(vlist)
        cls.check_child_use(records)
        return records


class CategorizedMixin(object):

    cost_category = fields.Many2One('cost.manage.category',
        'Cost Category', domain=[('childs', '=', None)])

    @property
    def cost_category_used(self):
        pool = Pool()
        Model = pool.get('ir.model')

        category = self.cost_category
        # Allow empty values on on_change
        if not category and not Transaction().readonly:
            model, = Model.search([('model', '=', self.__name__)])
            raise UserError(
                gettext('cost_manage.msg_cost_manage_missing_cost',
                    name=self.rec_name,
                    model=model.rec_name))

        return category


class CostDocumentableMixin(object):

    cost_override = fields.Boolean('Cost override', readonly=True)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['cost_override'] = True
        return super(CostDocumentableMixin, cls).copy(records, default=default)

    @staticmethod
    def default_cost_override():
        return True
