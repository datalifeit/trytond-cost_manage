# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from proteus import Model

__all__ = ['create_cost_allocation_pattern', 'get_cost_allocation_pattern']


def create_cost_allocation_pattern(config=None):
    Allocation = Model.get('cost.manage.cost.allocation.pattern')
    allocation = Allocation(code='01', name='Allocation pattern 1')
    allocation.save()


def get_cost_allocation_pattern(confing=None):
    Allocation = Model.get('cost.manage.cost.allocation.pattern')
    allocations = Allocation.find([])
    return allocations[0] if allocations else None
